import React from 'react';
import TestProxy from './components/TestProxy/TestProxy';

export default function Root(props) {
  return (
    <section>
      <TestProxy />
      {props.name} is mounted!
    </section>
  );
}
