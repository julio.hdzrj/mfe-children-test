import React, { useEffect } from 'react';

const TestProxy = () => {
  const getData = () => {
    fetch('https://apimocha.com/mfeapi/user/1', {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
        accept: '*/*',
        'Accept-Encoding': 'gzip, deflate, br',
        connection: 'keep-alive',
      },
    })
      .then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));

    // fetch('/user/1')
    //   .then(response => response.json())
    //   .then(data => console.log(data));
  };
  useEffect(() => {
    getData();
  });

  return (
    <div>
      <h1>TestProxy</h1>
    </div>
  );
};

export default TestProxy;
